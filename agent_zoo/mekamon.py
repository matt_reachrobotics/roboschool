import os.path, gym
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
import roboschool


def demo_run():

    env = gym.make("RoboschoolMekamon-v1")

    while 1:
        frame = 0
        score = 0
        restart_delay = 0
        obs = env.reset()


if __name__=="__main__":
    demo_run()
